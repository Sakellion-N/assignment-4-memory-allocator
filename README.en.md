# Assignment: Memory allocator
---
Laboratory work: Memory allocator


# Preparation

- Read about [automatic variables](https://www.gnu.org/software/make/manual/html_node/Automatic-Variables.html ) in the `Makefile`
- Read chapter 12 (pp. 235-239), chapter 13 (in its entirety) "Low-level programming: C, assembly and program execution".
- Virtual memory
- Refresh your knowledge about virtual memory and its organization (Chapter 4 "Low-level programming: C, assembly and program execution").
- Remember what file descriptors are. The system calls `open`, `close`, `write` and others work with them.
- Read carefully `map mmap`, especially the value of the `MAP_FIXED` and `MAP_FIXED_NOREPLACE` flags.
- Language capabilities. Make sure you are aware of how the following work:
- keywords `inline` (page 280 in the tutorial) and `restrict` (page 281 in the tutorial).
- Flexible array members (page 209 in the tutorial).
- The `offsetof` macro and especially [this page](https://p99.gforge.inria.fr/p99-html/group__flexible.html ).
- using structures to create new type aliases (as just `typedef`) but without implicit conversion (see [lesson on Stepik](https://stepik.org/lesson/408350/step/15 )).

At the defense, we can discuss any questions about these materials.

# Memory Allocator

We have repeatedly used the memory allocator, which is part of the standard library C.
Working with it is carried out through the functions `malloc` and `free` (as well as `calloc` and `realloc`).
To get a better feel of how it works, we will write our own simplified version of the allocator.

# Zero approximation

The memory allocator allows you to request blocks of arbitrary size, and then you can return these blocks to it in order to reuse memory.

The allocator reserves a large area of memory using `mmap`. He marks it up into blocks with headings, the headings form a connected list.
The header indicates whether the block is free, its size and who is its next block.

- `malloc(size_t n)` : we are looking for a free block of size at least `n` and divide it into two blocks:
- a block of size $`n`$
- the remaining part

At the same time, in the course of the search, we combine neighboring free blocks into free blocks of a larger size.
- When freeing memory, we mark the block as unoccupied and combine it with
the next block as long as possible (as long as both the current and the next block are free and
as long as the next block is in memory immediately after this one).
- If a large memory area runs out, we reserve more memory. At first
we are trying to do this closely, right after the last block, but if not
it turns out &mdash; we allow `mmap` to choose the appropriate address to start a new
region.



# First approximation

Let's imagine a fairly large area of memory that we allocate for a heap. Let's call it a *region*.
Let's mark up the region into blocks; each block starts with a header, and immediately after it comes the data.

``
|___heading1____|____date1___||___heading2____|____dating2___||___heading3____|____...
``

The blocks fill the entire space of the region.

## Title

The block header contains a link to the next block and a note about the block status (busy or free).

```c
/* mem_internals.h */

//See https://stepik.org/lesson/408350/step/15
typedef struct { size_t bytes; } block_capacity;

struct block_header {
struct block_header* next;
block_capacity capacity;
bool is_free;
uint8_t contents[]; // flexible array member
};
```
The heap is set by reference to the header of the first block.

## Size and capacity

Each unit has two characteristics: *size* and *capacity*. In order not to confuse them, we will create two types for them.

```c
/* mem_internals.h */
typedef struct { size_t bytes; } block_capacity;
typedef struct { size_t bytes; } block_size;
```

The block size is always on `offsetof(struct block_header, contents)` more than its capacity.

```c
/* mem_internals.h */
inline block_size size_from_capacity( block_capacity cap ) {
return (block_size) {cap.bytes + offsetof( struct block_header, contents ) };
}
inline block_capacity capacity_from_size( block_size sz ) {
return (block_capacity) {sz.bytes - offsetof( struct block_header, contents ) };
}
```

- The header stores the capacity of the block, not its total size together with the header.
- You cannot use `sizeof( struct block_header )`, because due to alignment, the size of the structure will be larger. On the author's machine, for example, the size of the structure was 24, and `offsetof( struct block_header, contents ) == 17`, which is correct.

## The `malloc(n)` algorithm

- We sort through the blocks until we find a "good" one.
A good block is one in which you can fit `n` bytes.
- If there is no good block, then see the second approximation.
- A good block may be too large, for example, we need to allocate 20 bytes, and its size is 30 megabytes. Then we divide the block into two parts: the first block will have `20 + offsetof( struct block_header, contents ) ` bytes.
The address of the contents of this block and will return `malloc`.

## Algorithm `free(void* addr)`

- If `addr == NULL`, then we don't do anything.
- We need to get from `addr` (which points to the beginning of the `contents` field) the address of the beginning of the header (to do this, subtract `sizeof(struct mem)` from it).
In the block header, set `is_free = true`, that's it.



# Second approximation

Now we will describe a few more aspects of allocation.

- what to do with a large number of consecutive free blocks?
- how to avoid the appearance of too small blocks?
- what should I do if the memory in the heap has run out?


## The `malloc(n)` algorithm

- It makes no sense to allocate a block of, say, 1 byte in size; even its header will take up more space.
Let the minimum capacity of the block be denoted as:
```c
#define BLOCK_MIN_CAPACITY 24
```

Too small blocks can form in two cases:
- `n < BLOCK_MIN_CAPACITY`. Then we will request a block not of size `n`, but of size `BLOCK_MIN_CAPACITY'.
- We found a good block, and its size is slightly larger than `n'. If you divide the block into two parts, the capacity of the second part will be less than `BLOCK_MIN_CAPACITY'. We will not divide such blocks, but we will give the whole block.

- When searching for a good block, we go through the blocks of the heap. Before deciding whether a block is good or not, let's combine it with all the free blocks following it.
- If the heap has run out of memory, it is necessary to expand the heap. To do this, we will use the `mmap` system call. Be sure to read `man` to understand with what arguments (`prot` and `flags`) to call it!

- First you need to try to allocate memory close to the end of the heap and mark it up into one large free block. If the last block was free in the first region of the heap, it is necessary to combine it with the next one.
- If it did not work out to select the region closely, then you need to select the region "where it will work". The last block of the previous region will be linked to the first block of the new region.


## Algorithm `free(void* addr)`

- In addition to what has already been written about `free`, when a block is released, you can combine it with all the free blocks following it.

# Task

- Implement the allocator using a blank in the repository.
- Come up with tests showing the work of the allocator in important cases:
- Normal successful memory allocation.
- Release of one block from several allocated ones.
- Release of two blocks from several allocated ones.
- Memory is over, the new memory region expands the old one.
- Memory has run out, the old memory region cannot be expanded due to a different allocated address range, the new region is allocated elsewhere.
Tests should be run from `main.c`, but can be described in separate (separate) files.
The algorithm is not the simplest, it is easy to make mistakes. In order not to waste time debugging, be sure to split into small functions!
- Figure out how the `Makefile` is written and fix it so that the compiled `main.c` is included in the final code. If you wish, you can write your own `Makefile`, but only if it is more beautiful and expressive.


# For self-checking


- Read [the rules of good style](https://gitlab.se.ifmo.ru/c-language/main/-/wikis/%D0%9F%D1%80%D0%B0%D0%B2%D0%B8%D0%BB%D0%B0-%D1%81%D1%82%D0%B8%D0%BB%D1%8F-%D0%BD%D0%B0%D0%BF%D0%B8%D1%81%D0%B0%D0%BD%D0%B8%D1%8F-%D0%BF%D1%80%D0%BE%D0%B3%D1%80%D0%B0%D0%BC%D0%BC-%D0%BD%D0%B0-C). Your solution
must match them.
- Check the architecture.
- Please send the solution in the form of a pull-request. [Instruction](https://gitlab.se.ifmo.ru/cse/main/-/wikis/%D0%9A%D0%B0%D0%BA-%D0%BF%D0%BE%D1%81%D0%BB%D0%B0%D1%82%D1%8C-%D0%B7%D0%B0%D0%B4%D0%B0%D0%BD%D0%B8%D0%B5-%D0%BD%D0%B0-%D0%BF%D1%80%D0%BE%D0%B2%D0%B5%D1%80%D0%BA%D1%83). In extreme cases, a link to the repository is allowed on https://gitlab.se.ifmo.ru or https://github.com .


# Additional materials

- Ulrich Drepper. ["What every programmer should know about memory"](https://people.freebsd.org/~lstewart/articles/cpumemory.pdf)
- [`man mmap` online](https://man7.org/linux/man-pages/man2/mmap.2.html)

- [Doug Lea's article on how the allocator works in `glibc`](http://gee.cs.oswego.edu/dl/html/malloc.html ). The current version of the allocator works according to a more complex algorithm.
- [Source code of one of the latest versions of the allocator in `glibc'. A lot of well-written comments](docs/malloc-impl.c)
