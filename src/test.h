#ifndef _TEST_H
#define _TEST_H
void test_successful_malloc(char *heap);
void test_free_one_block(char *heap);
void test_free_two_blocks(char *heap);
void test_grow_heap(char *heap);
void test_grow_heap_not_continous(char *heap);
#endif
