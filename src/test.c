#include "test.h"

#include <assert.h>

#include "mem.h"
#include "mem_internals.h"

static struct block_header* three_blocks(char *heap) {
  struct block_header* first = (struct block_header*)heap;
  *first = (struct block_header) {
    .capacity = {BLOCK_MIN_CAPACITY},
    .is_free = true,
    .next = (struct block_header*)(heap + size_from_capacity((block_capacity){BLOCK_MIN_CAPACITY}).bytes)
  };
  *(first->next) = (struct block_header) {
    .capacity = {BLOCK_MIN_CAPACITY},
    .is_free = true,
    .next = (struct block_header*)(heap + 2 * size_from_capacity((block_capacity){BLOCK_MIN_CAPACITY}).bytes)
  };
  *(first->next->next) = (struct block_header) {
    .capacity = {BLOCK_MIN_CAPACITY},
    .is_free = true,
    .next = NULL
  };
  return first;
}

void test_successful_malloc(char *heap) {
  assert(heap == HEAP_START);
  char *malloced = _malloc(BLOCK_MIN_CAPACITY);
  assert(malloced != NULL);
  struct block_header* block = block_get_header(malloced);
  assert(!block->is_free);
  assert(block->capacity.bytes == BLOCK_MIN_CAPACITY);
  assert(block->next == block_after(block));
}

void test_free_one_block(char *heap) {
  struct block_header* first = three_blocks(heap);
  first->next->is_free = false;
  first->next->next->is_free = false;
  _free(first->next->contents);
  assert(first->next->is_free);
  assert(first->next->next != NULL);
  assert(first->capacity.bytes == BLOCK_MIN_CAPACITY);
}

void test_free_two_blocks(char *heap) {
  struct block_header* first = three_blocks(heap);
  first->next->is_free = false;
  first->next->next->is_free = false;
  _free(first->next->next->contents);
  assert(first->next->next->is_free);
  _free(first->next->contents);
  assert(first->next->next == NULL);
  assert(first->next->capacity.bytes == BLOCK_MIN_CAPACITY + size_from_capacity((block_capacity){BLOCK_MIN_CAPACITY}).bytes);
}

void test_grow_heap(char *heap) {
  struct block_header* full = (struct block_header *)heap;
  full->is_free = false;
  full->capacity.bytes = capacity_from_size((block_size){REGION_MIN_SIZE}).bytes;
  struct block_header* new = grow_heap(full, 0);
  fprintf(stderr, "after = %p, new = %p\n", block_after(full), (void *)new);
  assert(full->next == new);
  assert(new->next == NULL);
  /* Actual capacity of new region could be larger due to page address alignment */
  assert(new->capacity.bytes >= capacity_from_size((block_size){REGION_MIN_SIZE}).bytes);
  /* Can't force OS to map memory after region reliably,
   * so this assert fails most of the time.
   * assert(block_after(full) == new); */
}

void test_grow_heap_not_continous(char *heap) {
  struct block_header* full = (struct block_header *)heap;
  full->is_free = false;
  full->capacity.bytes = REGION_MIN_SIZE;
  /* Try allocate a really big chunk of memory to more likely cause defragmentation */
  struct block_header* new = grow_heap(full, 1000 * REGION_MIN_SIZE);
  assert(new != NULL);
  assert(new->next == NULL);
  assert(full->next == new);
  assert(block_after(full) != new);
  assert(new->capacity.bytes >= 1000 * REGION_MIN_SIZE);
}
