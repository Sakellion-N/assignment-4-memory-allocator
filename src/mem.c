#define _DEFAULT_SOURCE

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

void debug_block(struct block_header* b, const char* fmt, ... );
void debug(const char* fmt, ... );

extern inline block_size size_from_capacity( block_capacity cap );
extern inline block_capacity capacity_from_size( block_size sz );

static bool            block_is_big_enough( size_t query, struct block_header* block ) { return block->capacity.bytes >= query; }
static size_t          pages_count   ( size_t mem )                      { return mem / getpagesize() + ((mem % getpagesize()) > 0); }
static size_t          round_pages   ( size_t mem )                      { return getpagesize() * pages_count( mem ) ; }

static void block_init( void* restrict addr, block_size block_sz, void* restrict next ) {
  *((struct block_header*)addr) = (struct block_header) {
    .next = next,
    .capacity = capacity_from_size(block_sz),
    .is_free = true
  };
}

static size_t region_actual_size( size_t query ) { return size_max( round_pages( query ), REGION_MIN_SIZE ); }

extern inline bool region_is_invalid( const struct region* r );



static void* map_pages(void const* addr, size_t length, int additional_flags) {
  return mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags , -1, 0 );
}

/*  allocate memory region and initialize it with a block */
static struct region alloc_region  ( void const * addr, size_t query ) {
  if (addr == NULL) {
    return (struct region){
      .addr = NULL
    };
  }
  size_t size = region_actual_size(size_from_capacity((block_capacity){query}).bytes);
  /* Using MAP_FIXED_NOREPLACE here to not overwrite existing regions */
  void *mapped_addr = map_pages(addr, size, MAP_FIXED_NOREPLACE);
  bool extends = true;
  if(mapped_addr == MAP_FAILED) {
    /* try to alloc region "somewhere". If it fails, there is not much to do. */
    mapped_addr = map_pages(NULL, size, 0);
    if (mapped_addr == MAP_FAILED) {
      return (struct region){
        .addr = NULL
      };
    }
    extends = false;
  }
  block_init(mapped_addr, (block_size){size}, NULL);
  return (struct region){
    .addr = mapped_addr,
    .size = size,
    .extends = extends
  };
}

void* heap_init( size_t initial ) {
  const struct region region = alloc_region( HEAP_START, initial );
  if ( region_is_invalid(&region) ) return NULL;

  return region.addr;
}

#define BLOCK_MIN_CAPACITY 24

/*  --- Separation of blocks (if the free block found is too large )--- */

static bool block_splittable( struct block_header* restrict block, size_t query) {
  return block-> is_free && query + offsetof( struct block_header, contents ) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

static bool split_if_too_big( struct block_header* block, size_t query ) {
  size_t cut_size = size_max(query, BLOCK_MIN_CAPACITY);
  if (!block_splittable(block, cut_size)) {
    return false;
  }
  size_t remainder = block->capacity.bytes - cut_size;
  block->capacity.bytes = cut_size;
  struct block_header* new_block = block_after(block);
  block_init((void*)new_block, (block_size){remainder}, block->next);
  block->next = new_block;
  return true;
}


/*  --- Merging of adjacent free blocks --- */

void* block_after( struct block_header const* block )              {
  return  (void*) (block->contents + block->capacity.bytes);
}
static bool blocks_continuous (
                               struct block_header const* fst,
                               struct block_header const* snd ) {
  return (void*)snd == block_after(fst);
}

static bool mergeable(struct block_header const* restrict fst, struct block_header const* restrict snd) {
  return fst->is_free && snd->is_free && blocks_continuous( fst, snd ) ;
}

static bool try_merge_with_next( struct block_header* block ) {
  struct block_header* next = block->next;
  if (next == NULL || !mergeable(block, next)) {
    return false;
  }

  block->next = next->next;
  block->capacity.bytes += size_from_capacity(next->capacity).bytes;
  return true;
}


/*  --- ... if the heap size is enough --- */

struct block_search_result {
  enum {BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED} type;
  struct block_header* block;
};


static struct block_search_result find_good_or_last  ( struct block_header* restrict block, size_t sz )    {
  if (!block) {
    return (struct block_search_result){BSR_CORRUPTED, NULL};
  }
  while (block->next) {
    if (!block->is_free) {
      continue;
    }
    while (try_merge_with_next(block)) {}
    split_if_too_big(block, sz);
    if (block_is_big_enough(sz, block)) {
      return (struct block_search_result){BSR_FOUND_GOOD_BLOCK, block};
    }
    block = block->next;
  }
  split_if_too_big(block, sz);
  return (struct block_search_result) {
    block->is_free && block_is_big_enough(sz, block) ? BSR_FOUND_GOOD_BLOCK : BSR_REACHED_END_NOT_FOUND,
    block
  };
}

/*  Try to allocate memory in the heap starting from the `block` block without trying to expand the heap
 It can be reused as soon as the heap is expanded. */
static struct block_search_result try_memalloc_existing ( size_t query, struct block_header* block ) {
  struct block_search_result result = find_good_or_last(block, query);
  if (result.type == BSR_FOUND_GOOD_BLOCK) {
    split_if_too_big(block, query);
    block->is_free = false;
  }

  return result;
}



struct block_header* grow_heap( struct block_header* restrict last, size_t query ) {
  if (!last) {
    return NULL;
  }
  size_t to_alloc;
  struct region new_region;
  if (last->is_free) {
    /* allocating additional memory for free block */
    to_alloc = last->capacity.bytes - query;
    new_region = alloc_region(block_after(last), to_alloc);
    if (new_region.extends) {
      last->capacity.bytes += query;
      return last;
    }
  } else {
    to_alloc = size_max(query, BLOCK_MIN_CAPACITY);
    new_region = alloc_region(block_after(last), to_alloc);
  }
  last->next = new_region.addr;
  return new_region.addr;
}

/*  Implements the main logic of malloc and returns the header of the selected block */
static struct block_header* memalloc( size_t query, struct block_header* heap_start) {
  struct block_search_result result = try_memalloc_existing(query, heap_start);
  if (result.type == BSR_FOUND_GOOD_BLOCK) {
    result.block->is_free = false;
    return result.block;
  } else if (result.type == BSR_REACHED_END_NOT_FOUND) {
    result = try_memalloc_existing(query, grow_heap(result.block, query));
  }
  return result.block;
}

void* _malloc( size_t query ) {
  struct block_header* const addr = memalloc( query, (struct block_header*) HEAP_START );
  if (addr) return addr->contents;
  else return NULL;
}

struct block_header* block_get_header(void* contents) {
  return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

void _free( void* mem ) {
  if (!mem) return ;
  struct block_header* header = block_get_header( mem );
  header->is_free = true;
  while (try_merge_with_next(header)) {}
}
