#include <stdio.h>

#include "mem_internals.h"
#include "mem.h"
#include "test.h"

static struct {
  const char *name;
  void (*func)(char*);
} tests[] = {
  {"successful malloc", test_successful_malloc},
  {"free_one_block", test_free_one_block},
  {"free_two_blocks", test_free_two_blocks},
  {"grow_heap", test_grow_heap},
  {"grow_heap_not_continous", test_grow_heap_not_continous},
};

int main()
{
  void *heap = heap_init(REGION_MIN_SIZE);
  fprintf(stderr, "heap = %p\n", heap);
  for (size_t i = 0; i < sizeof(tests) / 16; i++) {
    tests[i].func((char *)heap);
    fprintf(stderr, "Test passed: %s\n", tests[i].name);
  }
  return 0;
}
